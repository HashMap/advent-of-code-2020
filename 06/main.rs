use std::str::Chars;
use std::collections::HashSet;

fn main() {
//    let wut: usize = std::fs::read_to_string(&"./input.txt")
//        .unwrap()
//        .split("\n\n")
//        .map(|g| {
//            let repl = g.replace("\n", "");
//            let mut char_vec: Vec<char> = repl.chars().collect();
//            char_vec.sort();
//            char_vec.dedup();
//            println!("{:?}", char_vec);
//            char_vec.len()
//        })
//        .sum();
//
    let wut: usize = std::fs::read_to_string(&"./input.txt")
        .unwrap()
        .split("\n\n")
        .map(|g| {
            let splits = g.split("\n").collect::<Vec<&str>>();

            splits
                .into_iter()
                .map(|s| s.trim().replace("\n", ""))
                .map(|s| s.chars().collect::<HashSet<char>>())
                    .fold(None, |maybe_set1: Option<HashSet<char>>, set2| {
                    maybe_set1
                        .map(|set1| set1.intersection(&set2).map(|c| *c).collect())
                        .or(Some(set2))

                })
                .map(|w| {
                    println!("{:?}", w);
                    w
                })
                .unwrap()
                .len()
        })
        .sum();
    println!("{}", wut);
}

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let commands = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| {
            let (dir, num) = l.split_at(1);
            (dir.chars().next().unwrap(), num.parse::<isize>().unwrap())
        })
        .collect::<Vec<(char, isize)>>();

    println!("{:?}", commands);

    let (mut wx, mut wy) = (10isize, 1isize);
    let (mut sx, mut sy) = (0isize, 0isize);

    for (comm, num) in commands {
        match (comm) {
            'N' => wy += num,
            'S' => wy -= num,
            'E' => wx += num,
            'W' => wx -= num,
            'F' => {
                sx = sx + num * wx;
                sy = sy + num * wy;
            }
            'R' => {
                let (l, r) = rotate(wx, wy, num);
                wx = l;
                wy = r;
            }
            'L' => {
                let (l, r) = rotate(wx, wy, -num);
                wx = l; 
                wy = r; 
            }
            _ => println!("What? This is madness: {}", comm)
        }

        println!("wx: {}, wy: {}", wx, wy);
        println!("sx: {}, sy: {}", sx, sy);
        println!("");
    }
    println!("{}", sx.abs() + sy.abs());
} 

fn rotate(wx: isize, wy: isize, num:isize) -> (isize, isize) {
    match num {
        0 => (wx, wy),
        90 | -270 => (wy, -wx),
        180 | -180 => (-wx, -wy),
        270 | -90 => (-wy, wx),
        _ => panic!(),
    }
}

fn main_first() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let mut lines = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| {
            let mut chars = l.chars();
            let ins = chars.next().unwrap();
            let num = chars.collect::<String>().as_str().parse::<isize>().unwrap();
            (ins, num)
        })
    .collect::<Vec<(char, isize)>>();
    //println!("{:?}", lines);

    let mut x = 0;
    let mut y = 0;
    let mut deg = 90;

    for (ins, num) in lines {
        match ins {
            'N' => x += num,
            'S' => x -= num,
            'E' => y += num,
            'W' => y -= num,
            'L' => deg -= num,
            'R' => deg += num,
            'F' => match deg {
                0   => x += num,
                90  => y += num,
                180 => x -= num,
                270 => y -= num,
                _ => {
                    println!("{} {} {}", x, y, deg);
                    panic!("What is this ?")
                }
            }
            _ => {
                println!("{} {} {}", x, y, deg);
                panic!("What is this")
            }
        }
        deg = (deg + 360) % 360;// to keep it in [0,360)
    }

    println!("{}", x.abs() + y.abs());
}

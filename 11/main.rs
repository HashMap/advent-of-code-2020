use std::mem;
const WIDTH: usize = 92;
const HEIGHT: usize = 100;

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let lines = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .collect::<Vec<&str>>();

    let mut current = [['.'; WIDTH]; HEIGHT];

    for y in 1..HEIGHT-1 {
        for x in 1..WIDTH-1 {
            current[y][x] = lines[y].chars().nth(x).unwrap();
        }
    }


    loop {
        let mut next = [['.'; WIDTH]; HEIGHT];
        for y in 1..=HEIGHT-1 {
            for x in 1..=WIDTH-1 {
                match current[y][x] {
                    'L' => {
                        if count_r(x, y, '#', current) == 0 {
                            next[y][x] = '#';
                        } else {
                            next[y][x] = 'L';
                        }
                    }
                    '#' => {
                        let c = count_r(x, y, '#', current);
                        if c >= 5 {
                            next[y][x] = 'L';
                        } else {
                            next[y][x] = '#';
                        }
                    }
                    _ => () // do nothing
                }
            }
        }
        println!("After loop");
        print_state(current);
        print_state(next);
        if current == next {
            println!("{}", count_final(current));
            break;
        }
        current.copy_from_slice(&next[..]);
    }
}

fn count_r(curr_x: usize, curr_y: usize, ch: char, state: [[char;WIDTH];HEIGHT]) -> usize {
    let arr: [(isize, isize); 8] = [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (-1, -1), (1, -1), (-1, 1)];

    let mut c = 0;

    for (dir_x, dir_y) in arr.iter() {
        c+= count_ray(curr_x, curr_y, state, *dir_x, *dir_y);
    }

    return c;
}

fn count_ray(curr_x: usize, curr_y: usize, state: [[char;WIDTH];HEIGHT], dir_x: isize, dir_y: isize) -> usize {
    let mut x = (curr_x as isize + dir_x) as usize;
    let mut y = (curr_y as isize + dir_y) as usize;

    if !(1..=HEIGHT-1).contains(&y) ||
        !(1..=WIDTH-1).contains(&x) {
            return 0;
    }

    loop {
        let cha = state[y][x];
        match cha {
            '#' => return 1,
            'L' => return 0,
            _ => ()
        }

        x = (x as isize + dir_x) as usize;
        y = (y as isize + dir_y) as usize;
        if !(1..=HEIGHT-1).contains(&y) ||
            !(1..=WIDTH-1).contains(&x) {
                return 0;
        }
    }
    panic!("Shouldn't happen");

}



fn main_first() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let lines = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .collect::<Vec<&str>>();

    let mut current = [['.'; WIDTH]; HEIGHT];

    for y in 1..HEIGHT-1 {
        for x in 1..WIDTH-1 {
            current[y][x] = lines[y].chars().nth(x).unwrap();
        }
    }


    loop {
        let mut next = [['.'; WIDTH]; HEIGHT];
        for y in 1..=HEIGHT-1 {
            for x in 1..=WIDTH-1 {
                match current[y][x] {
                    'L' => {
                        if count(x, y, '#', current) == 0 {
                            next[y][x] = '#';
                        } else {
                            next[y][x] = 'L';
                        }
                    }
                    '#' => {
                        let c = count(x, y, '#', current);
                        if c >= 4 {
                            next[y][x] = 'L';
                        } else {
                            next[y][x] = '#';
                        }
                    }
                    _ => () // do nothing
                }
            }
        }
        println!("After loop");
        print_state(current);
        print_state(next);
        if current == next {
            println!("{}", count_final(current));
            break;
        }
        current.copy_from_slice(&next[..]);
    }

    //print_state(current);
} 

fn count(curr_x: usize, curr_y: usize, ch: char, state: [[char;WIDTH];HEIGHT]) -> usize {
    let mut c = 0;
    for y in curr_y-1..=curr_y+1 {
        for x in curr_x-1..=curr_x+1 {
            if curr_x == x && curr_y == y {
                continue;
            }
            if state[y][x] == ch {
                c += 1;
            }
        }
    }

    return c;
}

fn count_final(state: [[char;WIDTH];HEIGHT])-> usize {
    let mut c = 0;
    for y in 1..HEIGHT-1 {
        for x in 1..WIDTH-1 {
            if state[y][x] == '#' {
                c+=1;
            }
        }
    }
    return c;
}

fn reset(mut state: [[char;WIDTH];HEIGHT]) {
    for y in 1..HEIGHT-1 {
        for x in 1..WIDTH-1 {
            state[y][x] = '.';
        }
        println!();
    }
    println!();
}

fn print_state(state: [[char;WIDTH];HEIGHT]) {
    for y in 1..HEIGHT-1 {
        for x in 1..WIDTH-1 {
            print!("{}", state[y][x]);
        }
        println!();
    }
    println!();
}

fn main_first_2() {
    let s: String = std::fs::read_to_string(&"./input_test.txt").unwrap();
    let mut state_one = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| l.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();

    let mut state_two = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| l.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();

    for i in 0..state_one.len() {
        for j in 0..state_one[0].len() {
            let ch = state_one[i][j];
            print!("{}", ch);
            if ch == '.' {
                //do nothing on floor
                continue;
            }
            if ch == 'L' {
                //if no full around - set to occupied
            }
            if ch == '#' {
                //count full around - if 4 or more around - set to empty
            }
        }
        println!("");
    }
}

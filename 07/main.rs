use std::collections::HashMap;

#[derive(Debug)]
struct Child {
    name: String,
    count: usize,
}

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let filtered = s.split("\n")
        .into_iter()
        .filter(|l| !l.contains("no other"))
        .filter(|l| !l.is_empty())
        .map(|l| l.replace("bags", "").replace("bag", "").replace(".", ""));
    let mut map: HashMap<String, Vec<Child>> = HashMap::new();

    for line in filtered {
        let kv: Vec<&str> = line.split("contain").collect();
        let parent = kv[0].trim();
        let children_str = kv[1].trim();
        let children: Vec<Child> = children_str
            .split(",")
            .map(|ch| ch.trim())
            .map(|ch| {
                let index = ch.find(" ").unwrap();
                let (count, name) = ch.split_at(index);
                Child {
                    name: name.trim().into(),
                    count: count.parse::<usize>().unwrap()
                }
            })
            .collect();
        map.insert(parent.into(), children);
    }

    let count = count(map.get("shiny gold").unwrap(), &map);

    println!("{}", count);
}

fn count(children: &Vec<Child>, allMap: &HashMap<String, Vec<Child>>) -> usize {
    if children.len() > 0 {
        let mut result = 0;
        for child in children {
            result += child.count;
            let inner = count(allMap.get(&child.name).unwrap_or(&Vec::new()), allMap);
            result += child.count * inner;
        }
        return result;
    }
    return 0;
}

fn main_first() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let filtered = s.split("\n")
        .into_iter()
        .filter(|l| !l.contains("no other"))
        .filter(|l| !l.is_empty())
        .map(|l| l.replace("bags", "").replace("bag", "").replace(".", ""));
    let mut map: HashMap<String, Vec<Child>> = HashMap::new();

    for line in filtered {
        let kv: Vec<&str> = line.split("contain").collect();
        let parent = kv[0].trim();
        let children_str = kv[1].trim();
        let children: Vec<Child> = children_str
            .split(",")
            .map(|ch| ch.trim())
            .map(|ch| {
                let index = ch.find(" ").unwrap();
                let (count, name) = ch.split_at(index);
                Child {
                    name: name.trim().into(),
                    count: count.parse::<usize>().unwrap()
                }
            })
            .collect();
        map.insert(parent.into(), children);
    }

    let mut count = 0;

    for (key, value) in &map {
        if isShiny(key, value, &map) {
            println!("{} {:?}", key, value);
            count += 1;
        }
    }

    println!("{}", count);
}

fn isShiny(key: &String, children: &Vec<Child>, allMap: &HashMap<String, Vec<Child>>) -> bool {
    if children.len() > 0 {
        for child in children {
            if child.name == "shiny gold" {
                return true;
            }
            if isShiny(&child.name, allMap.get(&child.name).unwrap_or(&Vec::new()), allMap) {
                return true;
            }
        }
    }
    return false;
}

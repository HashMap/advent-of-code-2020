fn main() {
    let s: String = std::fs::read_to_string(&"./input_test.txt").unwrap();

    let mut mask: &str = "";

    for line in s.lines() {
        if line.starts_with("mask") {
            mask = &line[line.find('=').unwrap()+2..];
            println!("{}", mask);
        }

        if line.starts_with("mem") {
            let mem_index = &line[line.find("[").unwrap()+1..line.find("]").unwrap()];
            let value = &line[line.find('=').unwrap()+2..];

            println!("{} {}", mem_index, value);
        }
    }

}

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let mut nums = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    nums.sort();
    let max = max_val(&nums[..]) + 3;
    let mut one = 0;
    let mut curr_joltage = 0;
    let mut curr_chain = 1;
    let mut result: Vec<usize> = Vec::new();

    nums.insert(0, 0);
    nums.push(max);
    println!("{:?}", nums);
    for i in 0..nums.len() {
        let diff = nums[i] - curr_joltage;
        if diff == 1 {
            curr_chain += 1;
        }
        if diff == 3  {
            if curr_chain >= 3 {
                result.push(curr_chain);
            }
            curr_chain = 1;
        }
        curr_joltage = nums[i];
    }

    println!("{:?}", result);
    let comb = result.into_iter().map(|n| {
        let c = match n {
            1 => 1,
            2 => 1,
            3 => 2,
            4 => 4,
            5 => 7,
            _ => {
                println!("We have a chain larger than 5");
                0
            }
        };
        c
    })
    .filter(|n| n > &1)
    .map(|n| {println!("{}", n); n})
    .map(|n| n as u64)
    .fold(1, |a, b| a * b);

    println!("{:?}", comb);
}

fn main_first() {
    let s: String = std::fs::read_to_string(&"./input_test.txt").unwrap();
    let mut nums = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    nums.sort();
    println!("{:?}", nums);
    let max = max_val(&nums[..]) + 3;
    let mut one = 0;
    let mut three = 1;
    let mut curr_joltage = 0;

    for i in 0..nums.len() {
        let diff = nums[i] - curr_joltage;
        if diff == 1 {
            one += 1;
        }
        if diff == 3 {
            three += 1;
        }
        curr_joltage = nums[i];
    }

    println!("{}", one * three);
}

fn max_val(data: &[usize])-> usize {
    let mut max = 0;
    for num in data {
        if num > &max {
            max = *num;
        }
    }
    return max;
}

fn min_val(data: &[usize])-> usize {
    let mut min = usize::MAX;
    for num in data {
        if num < &min {
            min = *num;
        }
    }
    return min;
}

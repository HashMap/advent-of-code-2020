use std::collections::HashSet;

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let lines = s.split("\n").filter(|l| !l.is_empty()).collect::<Vec<&str>>();
    println!("{:?}", lines);
    for i in 0..lines.len() {
        let line = &lines[i];
        if line.contains("nop") || line.contains("jmp") {
            let new_line = switch(line);
            let mut new_lines = lines.to_vec();
            new_lines[i] = &new_line;

            works(&new_lines);
        } else {
            continue;
        }
    }
}

fn switch(line: &str) -> String {
    if line.contains("nop") {
        return line.replace("nop", "jmp");
    } else {
        return line.replace("jmp", "nop");
    }
}

fn works(lines: &Vec<&str>) -> bool {
    let mut visited: HashSet<isize> = HashSet::new();
    let mut current : isize = 0;
    let mut accum : isize = 0;
    loop {
        let (next, diff) = process_line(lines[current as usize]);
        visited.insert(current);
        current += next;
        accum += diff;
        if visited.get(&current).is_some() {
            return false;
        }
        if current as usize == lines.len() {
            println!("{}", accum);
            return true;
        }
    }
}

fn main_first() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let lines = s.split("\n").collect::<Vec<&str>>();
    let mut visited: HashSet<isize> = HashSet::new();
    let mut current : isize = 0;
    let mut accum : isize = 0;
    loop {
        //println!("{} {} {:?}", current, accum, visited);
        let (next, diff) = process_line(lines[current as usize]);
        visited.insert(current);
        current += next;
        accum += diff;
        if visited.get(&current).is_some() {
            break;
        }
    }
    println!("{}", accum);
}

fn process_line(line: &str) -> (isize, isize) {
    let (instruction, value_str) = line.split_at(3);
    let value = value_str.trim().parse::<isize>().unwrap();
    match instruction {
        "nop" => return (1, 0),
        "acc" => return (1, value),
        "jmp" => return (value, 0),
        _ => panic!("Unknown instruction")
    }
}


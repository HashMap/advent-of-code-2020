use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::u32;

fn main() {
    if let Ok(lines) = read_lines("./input.txt") {
        let mut full = Vec::new();
        for l in lines {
            let value = l.unwrap();
            let first = &value[..7];
            let ones = first.replace("F", "0");
            let zeroes = ones.replace("B", "1");
            let row = u32::from_str_radix(&zeroes, 2).unwrap();

            let seat = &value[7..];
            let seat_ones = seat.replace("R", "1");
            let seat_zeroes = seat_ones.replace("L", "0");
            let col = u32::from_str_radix(&seat_zeroes, 2).unwrap();

            let seat_id = row * 8 + col;
            full.push(seat_id);
        }
        full.sort();
        println!("{:?}", full);
        for i in 0..full.len()-1 {
            if full[i+1] != full[i] + 1 {
                println!("{}", full[i] + 1);
            }
        }
    }
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

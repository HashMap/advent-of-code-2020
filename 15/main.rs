use std::collections::HashMap;

const TURN: usize = 30000000;

fn main() {
    let mut map: HashMap<usize, usize> = HashMap::new();
    let mut numbers = Vec::new();

    numbers.push(0);
    numbers.push(14);
    numbers.push(1);
    numbers.push(3);
    numbers.push(7);
    numbers.push(9);

    map.insert(0, 0);
    map.insert(14, 1);
    map.insert(1, 2);
    map.insert(3, 3);
    map.insert(7, 4);
    map.insert(9, 5);


    //numbers.push(0);
    //set.insert(0);
    //numbers.push(14);
    //set.insert(14);
    //numbers.push(1);
    //set.insert(1);
    //numbers.push(3);
    //set.insert(3);
    //numbers.push(7);
    //set.insert(7);
    //numbers.push(9);
    //set.insert(9);
    

    //0,14,1,3,7,9

    let mut turn:usize = 5;
    //let mut turn:usize = 2;

    loop {
        turn += 1;
        let last_num = &numbers.last().cloned().unwrap();
        //println!("Last num: {}", last_num);
        //println!("Last occurence: {}", last_occurence_index);
        let was_spoken = map.contains_key(last_num);
        //println!("Was spoken: {}", was_spoken);

        if was_spoken {
            //println!("Last occurence: {}", last_occurence_index);
            let last_occurence_index = map[last_num];
            &numbers.push(turn - last_occurence_index-1);
        } else {
            &numbers.push(0);
        }

        &map.insert(*last_num, turn-1);

        if turn == TURN - 1{
            break;
        }
        if turn % 100000 == 0 {
            println!("Turn {}", turn);
        }
        //println!("{:?}", numbers);
    }

    println!("{}", numbers[numbers.len() - 1]);

}

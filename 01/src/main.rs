use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use itertools::Itertools;

fn main() {
    if let Ok(lines) = read_lines("./input.txt") {
        let ints: Vec<i32> = lines.map(|s| s.unwrap().parse::<i32>().unwrap()).collect();
        println!("{}", comb(ints, 2))
    }
}

fn comb(data: Vec<i32>, num: usize) -> i32 {
        let i = data.into_iter()
            .combinations(num)
            .into_iter()
            .filter(|vec| vec.iter().sum::<i32>() == 2020)
            .map(|vec| vec.iter().fold(1, |mut r, &val| {r = r * val; r}))
            .collect::<Vec<i32>>();
        *i.first().unwrap()
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

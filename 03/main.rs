use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    if let Ok(lines) = read_lines("./input.txt") {
        let width: usize = 30;
        let mut x : usize = 1;
        let mut tree: usize = 0;
        let right : usize = 1;
        for (i, line) in lines.enumerate() {
            if i == 0 { continue };
            if i % 2 != 0 { continue }
            let line_as_s = line.unwrap().replace('\n', "");
            let bytes = line_as_s.as_bytes();
            let c : char = bytes[x] as char;
            if c == '#' {
                tree = tree + 1;
            }
            println!("{}, {}, {}", line_as_s, x, c);
            if x + right <= width {
                x = x + right 
            } else {
                x = (x + (right - 1)) % width
            }
        }

        println!("{}", tree);
    }
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

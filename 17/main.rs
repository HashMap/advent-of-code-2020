use std::collections::HashSet;

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    println!("{}", s);

    let mut current: HashSet<(i8, i8, i8, i8)> = HashSet::new();

    for (y, line) in s.lines().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            if ch == '#' {
                current.insert((x as i8, y as i8, 0, 0));
            }
        }
    }

    //println!("{:?}", current);

    for _ in 0..6 {
        let mut next: HashSet<(i8, i8, i8, i8)> = HashSet::new();
        let mut to_test = HashSet::new();

        for cell in &current {
            for x in cell.0-1 ..=(cell.0+1) {
                for y in cell.1-1 ..=(cell.1+1) {
                    for z in cell.2-1 ..=(cell.2+1) {
                        for w in cell.3-1 ..=(cell.3+1) {
                            to_test.insert((x, y, z, w));
                        }
                    }
                }
            }
        }


        for cell in &to_test {
            let c = count(*cell, &current);
            
            //println!("{} {:?}", c, cell);
            if current.contains(cell) && (c == 2 || c == 3) {
                next.insert(*cell);
            }

            if !current.contains(cell) && c == 3 {
                next.insert(*cell);
            }
        }

        println!("Next len: {}", next.len());

        current = next;
    }
}

fn count(cell: (i8, i8, i8, i8), world: &HashSet<(i8, i8, i8, i8)>) -> usize {
    let mut count = 0;
    for x in cell.0-1 ..=(cell.0+1) {
        for y in cell.1-1 ..=(cell.1+1) {
            for z in cell.2-1 ..=(cell.2+1) {
                for w in cell.3-1 ..=(cell.3+1) {
                    if cell.0 == x && cell.1 == y && cell.2 == z && cell.3 == w {
                        continue;
                    }

                    if world.contains(&(x, y, z, w)) {
                        count += 1;
                    }
                }
            }
        }
    }

    return count;
}

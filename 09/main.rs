const PREAMBLE: usize = 25;
//const NUM: usize = 127;
const NUM: usize = 776203571;

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let nums = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    'outer: for i in 0..nums.len() {
        let mut num = nums[i];
        for j in i+1..nums.len() {
            num += nums[j];

            if num == NUM {
                let max = max_val(&nums[i..j]);
                let min = min_val(&nums[i..j]);
                println!("{}", min + max);
                break 'outer;
            }
            if num > NUM {
                continue 'outer;
            }
        }
    }
}

fn max_val(data: &[usize])-> usize {
    let mut max = 0;
    for num in data {
        if num > &max {
            max = *num;
        }
    }
    return max;
}

fn min_val(data: &[usize])-> usize {
    let mut min = usize::MAX;
    for num in data {
        if num < &min {
            min = *num;
        }
    }
    return min;
}


fn main_first() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    let nums = s.split("\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    println!("{:?}", nums);

    for i in (PREAMBLE)..nums.len() {
        let num = nums[i];
        let matrix_data = &nums[(i - PREAMBLE)..i];
        compute(matrix_data, num);
    }
}

fn compute(data: &[usize], val: usize) {
    let mut matrix : Vec<usize> = Vec::new();
    for i in 0..data.len() {
        for j in 0..data.len() {
            if i == j {
                continue;
            } else {
                matrix.push(data[i]+data[j]);
            }
        }
    }
    println!("{:?}, {}", matrix.contains(&val), &val);
}

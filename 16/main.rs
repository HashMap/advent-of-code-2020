use std::collections::HashMap;

#[derive(Debug)]
struct MinMax {
    min: usize,
    max: usize,
}

impl MinMax {
    fn valid(&self, num: usize) -> bool {
        return num >= self.min && num <= self.max;
    }
}

fn main() {
    let s: String = std::fs::read_to_string(&"./input.txt").unwrap();
    println!("{}", s);
    let mut groups = s.split("\n\n")
        .filter(|l| !l.is_empty())
        .into_iter()
        .collect::<Vec<&str>>();

    let rules_str = groups[0];
    let my_ticket_str = groups[1];
    let nearby_str = groups[2];

    let map = parse_rules(rules_str);

    let my_ticket = my_ticket_str.split("\n").into_iter().nth(1).unwrap().split(",").into_iter().map(|s| s.parse::<usize>().unwrap()).collect::<Vec<usize>>();
    let nearby = nearby_str.split("\n").into_iter().skip(1).map(|l| {
        let split = l.split(",").into_iter().filter(|s| !s.is_empty()).map(|s| s.parse::<usize>().unwrap()).collect::<Vec<usize>>();
        split
    }).collect::<Vec<Vec<usize>>>();

    let mut error = 0;

    let mut invalid_tickets = Vec::new();

    for (i, ticket) in nearby.iter().enumerate() {
         for num in ticket {
            let mut any = false;
            
            'inner: for rule in &map {
                let first_r = rule.1.valid(*num);
                let second_r = rule.2.valid(*num);

                if first_r || second_r {
                    any = true;
                    break 'inner;
                }
            }

            if !any {
                error += num;
                invalid_tickets.push(i);
            }
        }
    }

    //println!("{}", error);
    //println!("{:?}", invalid_tickets);

    let valid = nearby.iter().enumerate().filter(|(i, t)| !invalid_tickets.contains(i) && t.len() > 0).map(|(_, t)| t).collect::<Vec<&Vec<usize>>>();
    println!("{:?}", valid);


    //Lets compute ticket fields order - but how? :O

    let ticket_len = valid[0].len();

    let mut done = 0;
    let mut done_positions = Vec::new();
    let mut done_rules = Vec::new();
    let mut done_vec = Vec::new();
    loop {
        if &done == &map.len() {
            break;
        }

        println!("{:?}", done_rules);
    
        'rule: for rule in &map {
            println!("{:?}", rule);
            let mut possible = 0;
            let mut index = 0;

            'position: for i in 0..ticket_len {
                if done_positions.contains(&i) {
                    continue 'position;
                }

                for t in &valid {
                    let num = t[i];

                    if !rule.1.valid(num) && !rule.2.valid(num) {
                        continue 'position;
                    }
                }

                possible += 1;
                index = i;
            }

            println!("{}", possible);
            if possible == 1 {
                done_rules.push(&rule.0);
                done_positions.push(index);
                done_vec.push((&rule.0, index));
                done += 1;
            }
        }
    }


    println!("{:?}", done_vec);

    let departure = done_vec.into_iter().filter(|r| r.0.contains("departure")).map(|r| r.1).collect::<Vec<_>>();

    println!("{:?}", departure);

    let mut multiply = 1;

    for (i, num) in my_ticket.iter().enumerate() {
        if departure.contains(&i) {
            multiply = multiply * num;
        }
    }

    println!("{}", multiply);

}

fn parse_rules(rules_str: &str) -> Vec<(String, MinMax, MinMax)> {
    let vec = rules_str
        .split("\n")
        .map(|l| {
            let kv_split = l.split(":").into_iter().map(|s| s.to_string()).collect::<Vec<String>>();
            let first = &kv_split[0];
            let mut min_max_split = kv_split[1].split("or").into_iter().map(|s| s.trim()).map(|s| s.to_string()).collect::<Vec<String>>();
            let mut vec = vec![first.to_string()];
            vec.append(&mut min_max_split);
            vec
        }).collect::<Vec<_>>();
        
    let mut result = Vec::new();

    for rule in vec {
        let key = &rule[0];
        let first_min_max = rule[1].split("-").into_iter().collect::<Vec<&str>>();
        let first = MinMax {min:first_min_max[0].parse::<usize>().unwrap(), max:first_min_max[1].parse::<usize>().unwrap()};

        let second_min_max = rule[2].split("-").into_iter().collect::<Vec<&str>>();
        let second = MinMax {min:second_min_max[0].parse::<usize>().unwrap(), max:second_min_max[1].parse::<usize>().unwrap()};

        result.push((key.to_string(), first, second));
    }


    result
}

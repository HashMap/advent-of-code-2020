use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(Debug)]
struct PassTo {
    min: usize,
    max: usize,
    ch: char,
    pass: String

}

fn main() {
    if let Ok(lines) = read_lines("./input.txt") {
        let tos: Vec<PassTo> = lines.map(|s| parse(s.unwrap())).collect();
        let mut r : usize = 0;
        for to in tos {
            //let count: usize = count(&to.pass, to.ch);
            //if count <= to.max && count >= to.min {
            //    r += 1;
            //}
            let first = to.pass.as_bytes()[to.min - 1] as char;
            let second = to.pass.as_bytes()[to.max - 1] as char;
            //println!("{}, {}", first, second);
            if first == to.ch && second != to.ch || first != to.ch && second == to.ch {
                r += 1;
            }
        }
        println!("{}", r);
    }
}

fn count(pass: &String, exp: char) -> usize {
    let mut r : usize = 0;
    for ch in pass.chars() {
        if ch == exp {
            r = r + 1;
        }
    }
    r
}

fn parse(line: String) -> PassTo {
    let hyphen = line.find('-').unwrap();
    let space = line.find(' ').unwrap();
    let colon = line.find(':').unwrap();
    let min = &line[..hyphen].parse::<usize>().unwrap();
    let max = &line[hyphen+1..space].parse::<usize>().unwrap();
    let ch = &line[space+1..colon].parse::<char>().unwrap();
    let pass = &line[colon+2..];
    PassTo {
        min: *min, max: *max, ch: *ch, pass: pass.to_string()
    }
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

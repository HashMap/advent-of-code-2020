//const EARLIEST: usize = 939;
//const BUSES: &str = "7,13,x,x,59,x,31,19";
const EARLIEST: usize = 1000495;
const BUSES: &str = "19,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,521,x,x,x,x,x,x,x,23,x,x,x,x,x,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,29,x,523,x,x,x,x,x,37,x,x,x,x,x,x,13";

fn main() {
    let ids = BUSES.split(",").into_iter().map(|c| c.parse::<u64>().unwrap_or(0)).collect::<Vec<u64>>();

    let mut curr = 0;
    let mut step: u64 = 1;

    for (off, bus) in ids.iter().enumerate() {
        if *bus == 0 {
            continue;
        }

        for t in (curr..u64::MAX).step_by(step as usize) {
            if (t + off as u64) % bus == 0 {
                curr = t;
                step *= bus;
                break
            }
        }
    }

    println!("{}", curr);
}

fn main_first() {
    let ids = BUSES.split(",").into_iter().filter(|c| c != &"x").map(|c| c.parse::<usize>().unwrap()).collect::<Vec<usize>>();

    let mut timestamp = EARLIEST;

    loop {
        for id in &ids {
            if timestamp % id == 0 {
                println!("{}", (timestamp - EARLIEST) * id);
                return;
            }
        }
        timestamp+=1;
    }

}
